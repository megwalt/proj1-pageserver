# README #

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

### What do I need?  Where will it work? ###

* Designed for Unix, mostly interoperable on Linux (Ubuntu) or MacOS. May also work on Windows, but no promises. A Linux virtual machine may work, but our experience has not been good; you may want to test on shared server ix-dev.

* You will need Python version 3.4 or higher. 

* Designed to work in "user mode" (unprivileged), therefore using a port number above 1000 (rather than port 80 that a privileged web server would use)

* Windows 10 note: The new Windows bash on ubuntu looks promising. If you are running Windows 10, please give this a try and let me know if the Ubuntu/bash environment is suitable for CIS 322 develpment. 

### Assignment ###
* Author: Megan Walter, mwalter2@uoregon.edu
  
* Description: Extending a tiny web server in Python, gaining understanding of basic web architecture.

* Testing Instructions:

  ```
  git clone https://megwalt@bitbucket.org/megwalt/proj1-pageserver  <targetDirectory>
  ```
  
  ```
  cd <targetDirectory>
  ```
  
  ```
  make run or make start
  ```
  
  *test it with a browser now, while your server is running in a background process*

  ```
  make stop
  ```
  

* Maintained by Ram Durairajan, Steven Walton
 
